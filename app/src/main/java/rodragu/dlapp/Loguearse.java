package rodragu.dlapp;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Loguearse extends Activity {

	private Button entrar, registrarse;
	private EditText usuario, password;
	private TextView olvido;
	private ProgressDialog pDialog;
	private Httppostaux post;
	private String IP_Server = "localhost/dlapp/rest/web/"; // IP DEL SERVIDOR WEB
	private String URL_connect = "http://" + IP_Server + "login"; // Ruta en donde estan nuestros archivos

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loguearse);

		// DEFINIMOS LOS INTENTS
		final Intent i_n1_olvido = new Intent(this, Nivel1_Olvido.class);
		final Intent i_registrarse = new Intent(this, Nivel1_Registrarse.class);

		// LOCALIZAMOS LAS VARIABLES
		entrar = (Button) findViewById(R.id.btn_entrar);
		olvido = (TextView) findViewById(R.id.olvido_datos);
		registrarse = (Button) findViewById(R.id.btn_registrarse);
		usuario = (EditText) findViewById(R.id.edit_usuario);
		password = (EditText) findViewById(R.id.edit_pass);

		post = new Httppostaux();

		// EVENTOS DE BOTONES
		entrar.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {

				// PRIMERO CONTROLAMOS QUE NO HAYA NING�N CAMPO VAC�O
				if (usuario.getText().toString().equals("")
						|| password.getText().toString().equals("")) {
					error_login_V();
				} else {
					// CONSULTAMOS LOS DATOS EN LA BBDD Y SI NO HAY NING�N
					// RESULTADO DEVOLVEMOS UNA ALERTA

					asynclogin as = new asynclogin();
					as.execute(usuario.getText().toString(), password.getText()
							.toString());
				}
			}
		});

		olvido.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(i_n1_olvido);
			}
		});

		registrarse.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				startActivity(i_registrarse);
			}
		});
	}

	/*
	 * CLASE ASYNCTASK
	 * 
	 * usaremos esta para poder mostrar el dialogo de progreso mientras enviamos
	 * y obtenemos los datos podria hacerse lo mismo sin usar esto pero si el
	 * tiempo de respuesta es demasiado, lo que podria ocurrir si la conexion es
	 * lenta o el servidor tarda en responder la aplicacion sera inestable.
	 * ademas observariamos el mensaje de que la app no responde.
	 */

	class asynclogin extends AsyncTask<String, String, String> {

		String user, pass;

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Loguearse.this);
			pDialog.setMessage("Autenticando....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {
			// obtnemos usr y pass
			user = params[0];
			pass = params[1];

			// enviamos y recibimos y analizamos los datos en segundo plano.
			if (loginstatus(user, pass) == true) {
				return "ok"; // login valido
			} else {
				return "err"; // login invalido
			}
		}

		/*
		 * Una vez terminado doInBackground segun lo que halla ocurrido pasamos
		 * a la sig. activity o mostramos error
		 */
		protected void onPostExecute(String result) {

			pDialog.dismiss();// ocultamos progess dialog.

			Intent i_nivel1 = new Intent(Loguearse.this, Nivel1.class);

			Log.e("onPostExecute=", "" + result);

			if (result.equals("ok")) {
				i_nivel1.putExtra("usuario", user);
				startActivity(i_nivel1);

			} else {
				error_login();
			}
		}
	}

	// ////////////////////////////////////////////////////////

	/*
	 * Valida el estado del logueo solamente necesita como parametros el usuario
	 * y password
	 */
	public boolean loginstatus(String username, String password) {
		int logstatus = -1;

		/*
		 * Creamos un ArrayList del tipo nombre valor para agregar los datos
		 * recibidos por los parametros anteriores y enviarlo mediante POST a
		 * nuestro sistema para relizar la validacion
		 */
		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		postparameters2send.add(new BasicNameValuePair("usuario", username));
		postparameters2send.add(new BasicNameValuePair("password", password));

		// realizamos una peticion y como respuesta obtenemos un array JSON
		JSONArray jdata = post.getserverdata(postparameters2send, URL_connect);

		// si lo que obtuvimos no es null
		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data; // creamos un objeto JSON
			try {
				json_data = jdata.getJSONObject(0); // leemos el primer segmento en nuestro caso el unico accedemos al valor muestro por log que obtuvimos
				logstatus = json_data.getInt("logstatus");
				Log.e("loginstatus", "logstatus= " + logstatus);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// validamos el valor obtenido
			if (logstatus == 0) {// [{"logstatus":"0"}]
				Log.e("loginstatus ", "invalido");
				return false;
			} else {// [{"logstatus":"1"}]
				Log.e("loginstatus ", "valido");
				return true;
			}

		} else { // json obtenido invalido verificar parte WEB.
			Log.e("JSON  ", "ERROR");
			return false;
		}
	}

	// vibra y muestra un Toast
	public void error_login_V() {
		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(200);
		Toast.makeText(getApplicationContext(), R.string.campos_vacios,
				Toast.LENGTH_SHORT).show();
	}

	// vibra y muestra un Toast
	public void error_login() {
		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(200);
		Toast.makeText(getApplicationContext(), R.string.login_incorrecto,
				Toast.LENGTH_LONG).show();
	}
}