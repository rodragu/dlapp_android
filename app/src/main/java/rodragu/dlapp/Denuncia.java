package rodragu.dlapp;

public class Denuncia {
	
	//ATRIBUTOS
	private String nombre;
	private int cont;
	private int pos;
	
	//CONSTRUCTOR
	public Denuncia(String nombre)
	{
		this.nombre = nombre;
		this.cont = 0;
		this.pos = 0;
	}
	
	//M�TODOS
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void set_NumDenuncias(int num)
	{
		this.cont = num;
	}
	
	public int get_NumDenuncias()
	{	
		return this.cont;
	}	
	
	public void set_Posicion(int p)
	{
		this.pos = p;
	}
	
	public int get_Posicion()
	{
		return this.pos;
	}
}