package rodragu.dlapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

public class Nivel2_Ranking extends Activity {

	private ProgressDialog pDialog;
	private EditText p_trato, p_realizar, p_vulneracion, p_obligaciones,
			p_turno, p_ritmos, p_iom, n_trato, n_realizar, n_vulneracion,
			n_obligaciones, n_turno, n_ritmos, n_iom;
	private Denuncia d_trato, d_realizar, d_vulneracion, d_obligaciones,
			d_turno, d_ritmos, d_iom;
	private static ArrayList<Denuncia> Denuncias = new ArrayList<Denuncia>();
	private Httppostaux post;
	private String IP_Server = "www.denuncialaboralanonima.esy.es"; // IP DEL
																	// SERVIDOR
																	// WEB
	private String URL_connect = "http://" + IP_Server + "/ranking.php"; // RUTA
																			// EN
																			// DONDE
																			// EST�N
																			// NUESTROS
																			// ARCHIVOS

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel2_ranking);

		// CREAMOS EL OBJETO POST PARA REALIZAR LA CONEXI�N CON EL SERVIDOR
		post = new Httppostaux();

		asyncRanking as = new asyncRanking();
		as.execute();
	}

	// FUNCI�N QUE DEVUELVE EL NUMERO DE DENUNCIAS QUE SE HAN REALIZADO EN LA
	// APLIACI�N DEL TIPO QUE SE LE PASA POR PAR�METRO
	public int obtenerNumDenuncias(String Denuncia) {
		int num = 0;

		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		// PASAMOS EL VALOR "denuncia" MEDIANTE POST AL SERVIDOR
		postparameters2send.add(new BasicNameValuePair("denuncia", Denuncia));

		// realizamos una peticion y como respuesta obtenemos un array JSON
		JSONArray jdata = post.getserverdata(postparameters2send, URL_connect);

		// si lo que obtuvimos no es null
		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data; // creamos un objeto JSON
			try {
				json_data = jdata.getJSONObject(0); // leemos el primer segmento
													// en nuestro caso el unico
				num = json_data.getInt("denuncias");// accedemos al valor
				Log.e("denuncias", "Denuncias= " + num);// muestro
														// por
				// log que
				// obtuvimos
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return num;
	}

	class asyncRanking extends AsyncTask<String, String, String> {

		String user, pass;

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Nivel2_Ranking.this);
			pDialog.setMessage("Cargando Ranking....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			// INICIALIZAMOS LOS OBJETOS DENUNCIA
			d_trato = new Denuncia("TRATO INHUMANO N+X");
			d_realizar = new Denuncia("REALIZAR FUNCIONES DE OTRA CATEGOR�A");
			d_vulneracion = new Denuncia("VULNERACI�N DE DATOS");
			d_obligaciones = new Denuncia("OBLIGACIONES FUERA DE JORNADA");
			d_turno = new Denuncia("OBLIGACI�N DE CAMBIO DE TURNO");
			d_ritmos = new Denuncia("RITMOS DE TRABAJO ELEVADOS");
			d_iom = new Denuncia("TENGO IOM");

			// LOCALIZAMOS LAS VISTAS

			// PUESTOS EN EL RANKING
			p_trato = (EditText) findViewById(R.id.edit_p1);
			p_realizar = (EditText) findViewById(R.id.edit_p2);
			p_vulneracion = (EditText) findViewById(R.id.edit_p3);
			p_obligaciones = (EditText) findViewById(R.id.edit_p4);
			p_turno = (EditText) findViewById(R.id.edit_p5);
			p_ritmos = (EditText) findViewById(R.id.edit_p6);
			p_iom = (EditText) findViewById(R.id.edit_p7);

			// N�MEROS DE DENUNCIAS
			n_trato = (EditText) findViewById(R.id.edit_n1);
			n_realizar = (EditText) findViewById(R.id.edit_n2);
			n_vulneracion = (EditText) findViewById(R.id.edit_n3);
			n_obligaciones = (EditText) findViewById(R.id.edit_n4);
			n_turno = (EditText) findViewById(R.id.edit_n5);
			n_ritmos = (EditText) findViewById(R.id.edit_n6);
			n_iom = (EditText) findViewById(R.id.edit_n7);

			// INSERTAMOS EL N�MERO DE DENUNCIAS REALIZADAS EN CADA TIPO EN LOS
			// OBJETOS DENUNCIA

			d_trato.set_NumDenuncias(obtenerNumDenuncias(d_trato.getNombre()));
			d_realizar.set_NumDenuncias(obtenerNumDenuncias(d_realizar
					.getNombre()));
			d_vulneracion.set_NumDenuncias(obtenerNumDenuncias(d_vulneracion
					.getNombre()));
			d_obligaciones.set_NumDenuncias(obtenerNumDenuncias(d_obligaciones
					.getNombre()));
			d_turno.set_NumDenuncias(obtenerNumDenuncias(d_turno.getNombre()));
			d_ritmos.set_NumDenuncias(obtenerNumDenuncias(d_ritmos.getNombre()));
			d_iom.set_NumDenuncias(obtenerNumDenuncias(d_iom.getNombre()));

			return "ok";
		}

		/*
		 * Una vez terminado doInBackground segun lo que halla ocurrido pasamos
		 * a la sig. activity o mostramos error
		 */
		protected void onPostExecute(String result) {

			Log.e("onPostExecute=", "" + result);

			// ACTUALIZAMOS EL VALOR DE CADA TIPO DE DENUNCIA EN SU CASILLA

			n_trato.setText(Integer.toString(d_trato.get_NumDenuncias()));
			n_realizar.setText(Integer.toString(d_realizar.get_NumDenuncias()));
			n_vulneracion.setText(Integer.toString(d_vulneracion
					.get_NumDenuncias()));
			n_obligaciones.setText(Integer.toString(d_obligaciones
					.get_NumDenuncias()));
			n_turno.setText(Integer.toString(d_turno.get_NumDenuncias()));
			n_ritmos.setText(Integer.toString(d_ritmos.get_NumDenuncias()));
			n_iom.setText(Integer.toString(d_iom.get_NumDenuncias()));

			// INSERTAMOS LOS OBJETOS DENUNCIA EN UN ARRAYLIST PARA ORDENARLOS

			Denuncias.add(d_trato);
			Denuncias.add(d_realizar);
			Denuncias.add(d_vulneracion);
			Denuncias.add(d_obligaciones);
			Denuncias.add(d_turno);
			Denuncias.add(d_ritmos);
			Denuncias.add(d_iom);

			// FUNCI�N QUE NOS ORDENA UN ARRAYLIST

			Collections.sort(Denuncias, new Comparator<Denuncia>() {
				public int compare(Denuncia d1, Denuncia d2) {
					return Integer.valueOf(d2.get_NumDenuncias()).compareTo(
							Integer.valueOf(d1.get_NumDenuncias()));
				}
			});

			for (int i = 0; i < Denuncias.size(); i++) {
				Denuncias.get(i).set_Posicion(i);
			}

			p_trato.setText(Integer.toString(d_trato.get_Posicion() + 1) + "�");
			p_realizar.setText(Integer.toString(d_realizar.get_Posicion() + 1)
					+ "�");
			p_vulneracion
					.setText(Integer.toString(d_vulneracion.get_Posicion() + 1)
							+ "�");
			p_obligaciones.setText(Integer.toString(d_obligaciones
					.get_Posicion() + 1) + "�");
			p_turno.setText(Integer.toString(d_turno.get_Posicion() + 1) + "�");
			p_ritmos.setText(Integer.toString(d_ritmos.get_Posicion() + 1)
					+ "�");
			p_iom.setText(Integer.toString(d_iom.get_Posicion() + 1) + "�");

			Denuncias.removeAll(Denuncias);

			pDialog.dismiss();// ocultamos progess dialog.
		}
	}

	// onDestroy() Ir a nivel1
}