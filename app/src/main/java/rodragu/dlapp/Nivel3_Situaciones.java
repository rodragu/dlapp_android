package rodragu.dlapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Nivel3_Situaciones extends Activity {

	private Button trato, funciones, vulneracion, obligaciones, turno, ritmos,
			iom, solicitar;
	private String sede;
	private String usuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel3_denuncia_situacion);

		// Recogemos los parámetros pasados a la actividad mediante el
		// Intent
		Bundle extra = this.getIntent().getExtras();

		if (extra == null)
			return;

		sede = extra.getString("sede");
		usuario = extra.getString("usuario");
		
		//LOCALIZAMOS LAS VISTAS

		trato = (Button) findViewById(R.id.n3_btn_trato);
		funciones = (Button) findViewById(R.id.n3_btn_funciones);
		vulneracion = (Button) findViewById(R.id.n3_btn_vulneracion);
		obligaciones = (Button) findViewById(R.id.n3_btn_obligaciones);
		turno = (Button) findViewById(R.id.n3_btn_turno);
		ritmos = (Button) findViewById(R.id.n3_btn_ritmos);
		iom = (Button) findViewById(R.id.n3_btn_iom);
		solicitar = (Button) findViewById(R.id.n3_btn_solicitar);
		
		//INTENTS

		final Intent i_n4_trato = new Intent(this, Nivel4_Trato.class);
		final Intent i_n4_funciones = new Intent(this, Nivel4_Funciones.class);
		final Intent i_n4_vulneracion = new Intent(this,
				Nivel4_Vulneracion.class);
		final Intent i_n4_obligaciones = new Intent(this,
				Nivel4_Obligaciones.class);
		final Intent i_n4_turno = new Intent(this, Nivel4_Turno.class);
		final Intent i_n4_ritmos = new Intent(this, Nivel4_Ritmos.class);
		final Intent i_n4_iom = new Intent(this, Nivel4_Iom.class);
		final Intent i_n3_novedades = new Intent(this, Nivel2_Novedades.class);

		trato.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n4_trato.putExtra("sede", sede);
				i_n4_trato.putExtra("usuario", usuario);
				startActivity(i_n4_trato);
			}
		});

		funciones.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n4_funciones.putExtra("sede", sede);
				i_n4_funciones.putExtra("usuario", usuario);
				startActivity(i_n4_funciones);
			}
		});

		vulneracion.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n4_vulneracion.putExtra("sede", sede);
				i_n4_vulneracion.putExtra("usuario", usuario);
				startActivity(i_n4_vulneracion);
			}
		});

		obligaciones.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n4_obligaciones.putExtra("sede", sede);
				i_n4_obligaciones.putExtra("usuario", usuario);
				startActivity(i_n4_obligaciones);
			}
		});

		turno.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n4_turno.putExtra("sede", sede);
				i_n4_turno.putExtra("usuario", usuario);
				startActivity(i_n4_turno);
			}
		});

		ritmos.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n4_ritmos.putExtra("sede", sede);
				i_n4_ritmos.putExtra("usuario", usuario);
				startActivity(i_n4_ritmos);
			}
		});

		iom.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n4_iom.putExtra("sede", sede);
				i_n4_iom.putExtra("usuario", usuario);
				startActivity(i_n4_iom);
			}
		});

		solicitar.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				startActivity(i_n3_novedades);
			}
		});
	}
}