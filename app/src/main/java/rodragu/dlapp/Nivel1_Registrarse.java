package rodragu.dlapp;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Nivel1_Registrarse extends Activity {
	
	private Button registrarse;
	private EditText usuario, password, email;
	private ProgressDialog pDialog;
	private String IP_Server = "www.denuncialaboralanonima.esy.es"; // IP DEL SERVIDOR WEB
	private String URL_connect = "http://" + IP_Server; // RUTA EN DONDE EST�N
														// NUESTROS ARCHIVOS
	private Httppostaux post;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel1_registrarse);

		//VISTAS
		
		registrarse = (Button) findViewById(R.id.btn_registrarse);
		usuario = (EditText) findViewById(R.id.edit_usuario);
		password = (EditText) findViewById(R.id.edit_pass);
		email = (EditText ) findViewById(R.id.edit_email);

		// OBJETO PARA CONECTAR CON EL SERVIDOR
		post = new Httppostaux();

		registrarse.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {

				// PRIMERO CONTROLAMOS QUE NO HAYA NING�N CAMPO VAC�O

				if (usuario.getText().toString().equals("")
						|| password.getText().toString().equals("") || email.getText().toString().equals("")) {
					error_reg_V();
				} else {
					// SI YA HAY UN USUARIO REGISTRADO CON ESE NOMBRE
					// MOSTRAMOS UN MENSAJE DE ALERTA

					if (loginstatus(usuario.getText().toString(), password.getText().toString()) == true) {
						Toast.makeText(getApplicationContext(),
								R.string.usuario_existente, Toast.LENGTH_LONG)
								.show();
					} else {
						// SI NO HAY UN USUARIO REGISTRADO YA CON ESE NOMBRE, SE
						// REGISTRA EL NUEVO USUARIO EN LA BBDD Y MOSTRAMOS UNA
						// VENTANA
						// DICIENDO QUE EL USUARIO SE HA DADO DE ALTA
						// CORRECTAMENTE
						// Y ENTRAMOS EN EL MEN�

						asyncreg as = new asyncreg();
						as.execute(usuario.getText().toString(), password
								.getText().toString(), email.getText().toString());

					}
				}
			}
		});
	}

	/*
	 * CLASE ASYNCTASK
	 * 
	 * usaremos esta para poder mostrar el dialogo de progreso mientras enviamos
	 * y obtenemos los datos podria hacerse lo mismo sin usar esto pero si el
	 * tiempo de respuesta es demasiado lo que podria ocurrir si la conexion es
	 * lenta o el servidor tarda en responder la aplicacion sera inestable.
	 * ademas observariamos el mensaje de que la app no responde.
	 */

	class asyncreg extends AsyncTask<String, String, String> {

		String user, pass, mail;

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Nivel1_Registrarse.this);
			pDialog.setMessage("Registrando....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... params) {
			// obtnemos usr y pass
			user = params[0];
			pass = params[1];
			mail = params[2];

			// enviamos y recibimos y analizamos los datos en segundo plano.
			if (insertarUsuario(user, pass, mail) == true) {
				return "ok"; // Registro valido
			} else {
				return "err"; // Registro invalido
			}
		}

		/*
		 * Una vez terminado doInBackground segun lo que halla ocurrido pasamos
		 * a la sig. activity o mostramos error
		 */
		protected void onPostExecute(String result) {

			pDialog.dismiss();// ocultamos progess dialog.

			Intent i_nivel1 = new Intent(Nivel1_Registrarse.this, Nivel1.class);

			Log.e("onPostExecute=", "" + result);

			if (result.equals("ok")) {
				Toast.makeText(getApplicationContext(), "USUARIO REGISTRADO", Toast.LENGTH_LONG).show();
				i_nivel1.putExtra("usuario", user);
				startActivity(i_nivel1);

			} else {
				error_reg_V();
			}
		}
	}

	/*
	 * Valida el estado del logueo solamente necesita como parametros el usuario
	 * y password
	 */
	public boolean loginstatus(String username, String password) {
		int logstatus = -1;

		/*
		 * Creamos un ArrayList del tipo nombre valor para agregar los datos
		 * recibidos por los parametros anteriores y enviarlo mediante POST a
		 * nuestro sistema para relizar la validacion
		 */
		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		postparameters2send.add(new BasicNameValuePair("usuario", username));
		postparameters2send.add(new BasicNameValuePair("password", password));

		// realizamos una peticion y como respuesta obtenemos un array JSON
		JSONArray jdata = post.getserverdata(postparameters2send, URL_connect + "/acces.php");

		// si lo que obtuvimos no es null
		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data; // creamos un objeto JSON
			try {
				json_data = jdata.getJSONObject(0); // leemos el primer segmento
													// en nuestro caso el unico
				logstatus = json_data.getInt("logstatus");// accedemos al valor
				Log.e("loginstatus", "logstatus= " + logstatus);// muestro por
																// log que
																// obtuvimos
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// validamos el valor obtenido
			if (logstatus == 0) {// [{"logstatus":"0"}]
				Log.e("loginstatus ", "invalido");
				return false;
			}

			else

			{// [{"logstatus":"1"}]
				Log.e("loginstatus ", "valido");
				return true;
			}

		} else { // json obtenido invalido verificar parte WEB.
			Log.e("JSON  LOGSTATUS", "ERROR");
			return false;
		}
	}

	public boolean insertarUsuario(String user, String pass, String mail) {

		int logstatus = -1;

		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		postparameters2send.add(new BasicNameValuePair("usuario", user));
		postparameters2send.add(new BasicNameValuePair("password", pass));
		postparameters2send.add(new BasicNameValuePair("email", mail));

		// realizamos una peticion y como respuesta obtenemos un array JSON

		JSONArray jdata = post.getserverdata(postparameters2send, URL_connect + "/adduser.php");

		// si lo que obtuvimos no es null
		if (jdata != null && jdata.length() > 0) {

			JSONObject json_data; // creamos un objeto JSON
			try {
				json_data = jdata.getJSONObject(0); // leemos el primer segmento
													// en nuestro caso el unico
				logstatus = json_data.getInt("logstatus");// accedemos al valor
				Log.e("loginstatus", "logstatus= " + logstatus);// muestro por
																// log que
																// obtuvimos
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// validamos el valor obtenido
			if (logstatus == 0) {// [{"logstatus":"0"}]

				Log.e("loginstatus ", "Registro valido");
				return true;
			}

			else

			{// [{"logstatus":"1"}]
				error_reg();
				Log.e("loginstatus ", "Registro invalido");
				return false;
			}

		} else { // json obtenido invalido verificar parte WEB.
			Log.e("JSON  REGSTATUS", "ERROR");
			return false;
		}
	}

	// ERROR CAMPOS VACÍOS
	public void error_reg_V() {
		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(200);
		Toast.makeText(getApplicationContext(), R.string.campos_vacios_mas_email,
				Toast.LENGTH_SHORT).show();
	}

	// ERROR USUARIO YA EXITE
	public void error_reg() {
		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(200);
		Toast.makeText(getApplicationContext(), R.string.usuario_existente,
				Toast.LENGTH_LONG).show();
	}
}