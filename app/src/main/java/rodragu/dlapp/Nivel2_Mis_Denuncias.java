package rodragu.dlapp;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

public class Nivel2_Mis_Denuncias extends Activity {

	private static final int CENTER_HORIZONTAL = 0;
	private TableLayout tb;
	// private ResultSet c;
	private ProgressDialog pDialog;
	private String usuario, descripcion;
	private Httppostaux post;
	private String IP_Server = "www.denuncialaboralanonima.esy.es"; // IP DEL
																	// SERVIDOR
																	// WEB
	private String URL_connect = "http://" + IP_Server
			+ "/get_Mis_Denuncias.php"; // RUTA EN DONDE EST�N NUESTROS ARCHIVOS

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel2_mis_denuncias);

		// RECOGEMOS LOS PAR�METROS PASADO MEDIANTE INTENT
		Bundle extra = this.getIntent().getExtras();

		if (extra != null)
			usuario = extra.getString("usuario");

		asyncGet as = new asyncGet();
		as.execute(usuario, URL_connect);
	}

	class asyncGet extends AsyncTask<String, String, JSONArray> {

		String user, url;

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Nivel2_Mis_Denuncias.this);
			pDialog.setMessage("Descargando mis denuncias....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected JSONArray doInBackground(String... params) {

			// INICIALIZAMOS POST PARA OBTENER LA CONEXI�N CON EL SERVIDOR
			post = new Httppostaux();

			user = params[0];
			url = params[1];

			// Enviamos y recibimos y analizamos los datos en segundo plano

			/*
			 * Creamos un ArrayList del tipo nombre valor para agregar los datos
			 * recibidos por los parametros anteriores y enviarlo mediante POST
			 * a nuestro sistema para relizar la validacion
			 */

			ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

			postparameters2send.add(new BasicNameValuePair("usuario", user));

			// realizamos una peticion y como respuesta obtenemos un array JSON
			JSONArray jdata = post.getserverdata(postparameters2send, url);

			return jdata;
		}

		/*
		 * Una vez terminado doInBackground segun lo que halla ocurrido pasamos
		 * a la sig. activity o mostramos error
		 */
		protected void onPostExecute(JSONArray jdata) {

			pDialog.dismiss();// ocultamos progess dialog.

			String fecha, tipo, info;

			// si lo que obtuvimos no es null
			if (jdata != null && jdata.length() > 0) {
				JSONObject json_data; // creamos un objeto JSON

				for (int i = 0; i < jdata.length(); i++) {
					try {
						json_data = jdata.getJSONObject(i); // LEEMOS LOS
															// REGISTROS QUE
															// TIENE EL ARRAY DE
															// DENUNCIAS

						fecha = json_data.getString("fecha");// accedemos al
																// valor
						tipo = json_data.getString("tipo");
						info = json_data.getString("info");// accedemos al valor

						// CREAMOS EL INTENT PARA VER LA DENUNCIA COMPLETA

						final Intent i_n3_denuncia = new Intent(
								Nivel2_Mis_Denuncias.this,
								Nivel3_Denuncia.class);
						
						

						// IDENTIFICAMOS LA TABLA DONDE SE MOSTRAR�N LAS
						// DENUNCIAS
						tb = (TableLayout) findViewById(R.id.tb_mis_denuncias);
						
						//ESTABLECEMOS LAS PROPIEDADES DEL TABLELAYOUT
						
						LayoutParams layoutParams = new TableRow.LayoutParams(android.widget.TableRow.LayoutParams.MATCH_PARENT, android.widget.TableRow.LayoutParams.WRAP_CONTENT);
						LayoutParams layoutParams2 = new LayoutParams(android.widget.TableRow.LayoutParams.MATCH_PARENT, android.widget.TableRow.LayoutParams.WRAP_CONTENT);
						// MOSTRAMOS LOS REGISTROS DE LAS DENUNCIAS QUE HA
						// REALIZADO EL USUARIO

						TableRow row;

						row = new TableRow(Nivel2_Mis_Denuncias.this);
						row.setLayoutParams(layoutParams);

						final TextView date = new TextView(Nivel2_Mis_Denuncias.this);
						
						date.setPadding(20, 10, 10, 10);
						date.setText(fecha);
						date.setTextColor(-1);
						date.setGravity(CENTER_HORIZONTAL);
						row.addView(date);

						final TextView t_denun = new TextView(Nivel2_Mis_Denuncias.this);
						
						t_denun.setLayoutParams(layoutParams2);
						t_denun.setPadding(10, 10, 10, 20);
						t_denun.setText(tipo);
						t_denun.setTextColor(-1);
						t_denun.setGravity(CENTER_HORIZONTAL);
						row.addView(t_denun);

						final TextView descripcion = new TextView(Nivel2_Mis_Denuncias.this);
						
						descripcion.setText(info);
						descripcion.setEnabled(false);

						row.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								i_n3_denuncia.putExtra("fecha", date.getText()
										.toString());
								i_n3_denuncia.putExtra("tipo", t_denun
										.getText().toString());
								i_n3_denuncia.putExtra("descripcion",
										descripcion.getText().toString());
								startActivity(i_n3_denuncia);
							}
						});

						tb.addView(row);

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

			}
		}
	}
}