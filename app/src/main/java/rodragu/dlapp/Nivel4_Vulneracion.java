package rodragu.dlapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Nivel4_Vulneracion extends Activity {
	
	private Button denunciar;
	private String sede, usuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel4_vulneracion_datos);
		
		//Recogemos los parámetros pasados a la actividad mediante el Intent
		Bundle extra = this.getIntent().getExtras();
						
		if(extra == null) return;
		
		usuario = extra.getString("usuario");
		sede = extra.getString("sede");
		
		denunciar = (Button)findViewById(R.id.n4_btn_denuncia);
		
		final Intent i_n5_Denuncia = new Intent(this, Nivel5_Denunciar.class);
		
		denunciar.setOnClickListener(new OnClickListener(){
			public void onClick(View view) {
				i_n5_Denuncia.putExtra("sede", sede);
				i_n5_Denuncia.putExtra("usuario", usuario);
				i_n5_Denuncia.putExtra("tipo","Vulneración de datos");
				startActivity(i_n5_Denuncia);
			}
		});
	}
}