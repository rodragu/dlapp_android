package rodragu.dlapp;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/*CLASE AUXILIAR PARA EL ENVIO DE PETICIONES A NUESTRO SISTEMA
 * Y MANEJO DE RESPUESTA.*/

public class Httppostaux {
	
	/////////////
	//ATRIBUTOS//
	/////////////
	
	
	//Context contexto = new Context(this);
	InputStream inputS = null;
	String result = "";
	
	//////////
	//CLASES//
	//////////
	
	public JSONArray getserverdata(ArrayList<NameValuePair> parameters, String urlwebserver) {

		// conecta via http y envia un post.
		httppostconnect(parameters, urlwebserver);

		if (inputS != null) {// si obtuvo una respuesta

			getpostresponse();

			return getjsonarray();

		} else {

			return null;
		}
	}
	
	// peticion HTTP
	public String httppostconnect(ArrayList<NameValuePair> parametros, String urlwebserver) {

		String con; 
		try 
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(urlwebserver);
			httppost.setEntity(new UrlEncodedFormEntity(parametros));
			// ejecuto peticion enviando datos por POST
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			inputS = entity.getContent();
			Log.i("log_tag","Conectado!!");
			con = "Conectado";

		} 
		catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
			con = "No Conectado";
		}
		
		return con;
	}

	public void getpostresponse() {

		// Convierte respuesta a String
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputS, "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			inputS.close();
			
			result = sb.toString();
			Log.e("getpostresponse", " result= " + sb.toString());
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
	}

	public JSONArray getjsonarray() {
		// parse json data
		try {
			JSONArray jArray = new JSONArray(result);

			return jArray;
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
			return null;
		}
	}
}
