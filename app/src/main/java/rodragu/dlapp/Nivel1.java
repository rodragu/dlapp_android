package rodragu.dlapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class Nivel1 extends Activity {

	private Button denunciar, avisa, no_contacto, ranking, mis_denuncias,
			novedades, desconectar;
	private String usuario;
	private AlertDialog.Builder ventana;
	private static final int CONTACTAR = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel1);

		// Recogemos los parámetros pasados a la actividad mediante el
		// Intent
		Bundle extra = this.getIntent().getExtras();

		if (extra == null)
			return;

		usuario = extra.getString("usuario");

		// Localizamos las vistas de los botones
		denunciar = (Button) findViewById(R.id.n2_btn_denunciar);
		avisa = (Button) findViewById(R.id.n2_btn_avisa);
		no_contacto = (Button) findViewById(R.id.n2_btn_no_contacto);
		ranking = (Button) findViewById(R.id.n2_btn_ranking);
		mis_denuncias = (Button) findViewById(R.id.n2_btn_mis_denuncias);
		novedades = (Button) findViewById(R.id.n2_btn_novedades);
		desconectar = (Button) findViewById(R.id.n2_btn_desconectar);

		final Intent i_n2_ranking = new Intent(this, Nivel2_Ranking.class);
		final Intent i_n2_mis_denuncias = new Intent(this,
				Nivel2_Mis_Denuncias.class);
		final Intent i_n2_sede = new Intent(this, Nivel2_Sedes.class);
		final Intent i_n3_novedades = new Intent(this, Nivel2_Novedades.class);
		final Intent i_n3_desconectar = new Intent(this, Main.class);
		//final ProgressDialog pDialog = new ProgressDialog(Nivel1.this);

		denunciar.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n2_sede.putExtra("opcion", 0);
				i_n2_sede.putExtra("usuario", usuario);
				startActivity(i_n2_sede);
			}
		});

		avisa.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n2_sede.putExtra("opcion", 1);
				i_n2_sede.putExtra("usuario", usuario);
				startActivity(i_n2_sede);
			}
		});

		no_contacto.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				showDialog(CONTACTAR);
			}
		});

		ranking.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n2_ranking.putExtra("usuario", usuario);
				startActivity(i_n2_ranking);
			}
		});

		mis_denuncias.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n2_mis_denuncias.putExtra("usuario", usuario);
				startActivity(i_n2_mis_denuncias);
			}
		});

		novedades.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				i_n3_novedades.putExtra("usuario", usuario);
				startActivity(i_n3_novedades);
			}
		});

		desconectar.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				startActivity(i_n3_desconectar);
			}
		});
	}

	public Dialog onCreateDialog(int id) {
		final Intent i_enviar_email = new Intent(this,
				Nivel2_Enviar_Email.class);

		switch (id) {

		case CONTACTAR:

			ventana = new AlertDialog.Builder(this);
			ventana.setIcon(android.R.drawable.ic_dialog_alert);
			ventana.setTitle(R.string.tit_v_contactar);
			ventana.setMessage(R.string.n3_txt_contacta);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int opcion) {
							i_enviar_email.putExtra("usuario", usuario);
							startActivity(i_enviar_email);
						}
					});
		}

		ventana.show();
		return ventana.create();
	}
}