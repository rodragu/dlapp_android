package rodragu.dlapp;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Nivel2_Novedades extends Activity {

	private static final int ENVIADO = 0, VACIO = 1;
	private Button entrar;
	private EditText ed_enviar_sit;
	private AlertDialog.Builder ventana;
	private ProgressDialog pDialog;
	private Httppostaux post;
	private String IP_Server = "www.denuncialaboralanonima.esy.es";
	private String URL_connect = "http://" + IP_Server + "/addsituacion.php";
	private String situacion, con, usuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel2_novedades);

		// RECOGEMOS LOS PARÁMETROS PASADO MEDIANTE INTENT
		Bundle extra = this.getIntent().getExtras();

		if (extra == null)
			return;

		usuario = extra.getString("usuario");

		//Toast.makeText(getApplicationContext(), "Usuario: " + usuario,Toast.LENGTH_LONG).show();

		// INICIALIZAMOS POST PARA OBTENER LA CONEXIÓN CON EL SERVIDOR
		post = new Httppostaux();

		// LOCALIZAMOS LAS VISTAS

		entrar = (Button) findViewById(R.id.n3_btn_nov_env);
		ed_enviar_sit = (EditText) findViewById(R.id.edit_enviar_sit);

		entrar.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {

				if (ed_enviar_sit.getText().toString().equals("")) {
					showDialog(VACIO);

				} else {

					asyncSit as = new asyncSit();
					as.execute(ed_enviar_sit.getText().toString());
				}
			}
		});
	}

	class asyncSit extends AsyncTask<String, String, String> {

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Nivel2_Novedades.this);
			pDialog.setMessage("Insertando nueva situación....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... params) {

			situacion = params[0];
			// enviamos y recibimos y analizamos los datos en segundo plano.

			con = insertarSituacion(ed_enviar_sit.getText().toString());

			return con;
		}

		/*
		 * Una vez terminado doInBackground segun lo que halla ocurrido pasamos
		 * a la sig. activity o mostramos error
		 */

		protected void onPostExecute(String result) {

			pDialog.dismiss();// ocultamos progress dialog.

			if (con == "Conectado") {
				showDialog(ENVIADO);
			}
		}
	}

	public String insertarSituacion(String situacion) {

		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		postparameters2send.add(new BasicNameValuePair("situacion", situacion));

		// Enviamos la petición de inserción al servidor
		return post.httppostconnect(postparameters2send, URL_connect);
	}

	public Dialog onCreateDialog(int id) {

		switch (id) {

		case ENVIADO:

			ventana = new AlertDialog.Builder(this);
			ventana.setTitle(R.string.tit_v_enviado);
			ventana.setMessage(R.string.txt_mensaje_enviado);
			ventana.setIcon(android.R.drawable.ic_dialog_info);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {
							Intent i_nivel1 = new Intent(Nivel2_Novedades.this,
									Nivel1.class);
							i_nivel1.putExtra("usuario", usuario);
							startActivity(i_nivel1);
						}
					});
			break;

		case VACIO:

			ventana = new AlertDialog.Builder(this);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.txt_situacion_vacio);
			ventana.setIcon(android.R.drawable.ic_dialog_alert);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {
						}
					});
			break;
		}

		ventana.show();
		return ventana.create();
	}
}