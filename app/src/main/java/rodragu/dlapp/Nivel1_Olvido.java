package rodragu.dlapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Nivel1_Olvido extends Activity {
	
	private static final int VACIO = 0;
	private static final int ENVIADO = 1;
	private static final int NO_CORREO = 2;
	private String de, txt;
	private EditText ed_email;
	private Button btn_enviar;
	//private BDAdapter BDhelper;
	private AlertDialog.Builder ventana;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel1_olvido);
		
		//LOCALIZAMOS LAS VISTAS
		ed_email = (EditText)findViewById(R.id.ed_email);
		btn_enviar = (Button)findViewById(R.id.n2_btn_enviar_email);
		
		//INTENT PARA ENVIAR EL EMAIL
		final Intent i_email = new Intent(Intent.ACTION_SEND);
		
 		String [] to = {"denuncialaboralanonima@gmail.com"};
		
        i_email.setData(Uri.parse("mailto:"));
        i_email.putExtra(Intent.EXTRA_EMAIL, to);
        i_email.putExtra(Intent.EXTRA_SUBJECT, "Solicitud de datos de usuario y contrasena por olvido");
        i_email.putExtra(Intent.EXTRA_TEXT, txt);
        i_email.setType("message/rfc822");
        
        btn_enviar.setOnClickListener(new OnClickListener(){
			public void onClick(View view) {	
				if(ed_email.getText().toString().equals(""))
				{
					showDialog(VACIO);
				}
				else
				{
					try{
						
						txt = "Por favor, contacten conmigo en el siguiente email: " + ed_email.getText().toString();
						
						// INTENT PARA ENVIAR EL EMAIL
						final Intent i_email = new Intent(Intent.ACTION_SEND);
						
						final String[] to = {"denuncialaboralanonima@gmail.com"};
						
						i_email.setData(Uri.parse("mailto:"));
						i_email.putExtra(Intent.EXTRA_EMAIL, to);
						i_email.putExtra(Intent.EXTRA_SUBJECT,
								"Solicitud de contacto por email");
						i_email.putExtra(Intent.EXTRA_TEXT, txt);
						//i_email.setType("plain/text");
						i_email.setType("message/rfc822");
						
						startActivity(Intent.createChooser(i_email, "Send mail..."));
						showDialog(ENVIADO);
					} catch (android.content.ActivityNotFoundException ex) {
							showDialog(NO_CORREO);
						}
					}
					
				}
        });
	}
	
	public Dialog onCreateDialog(int id){
		switch (id) {

		case VACIO:
			
			ventana = new AlertDialog.Builder(this);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.txt_email_vacio);
			ventana.setIcon(android.R.drawable.ic_dialog_info);
			ventana.setPositiveButton(R.string.btn_aceptar, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int boton) {
					
				}
			});
			break;
		
		case ENVIADO:
			
			 //INTENT PARA VOLVER AL PRINCIPIO DE LA APP
	        final Intent i_main = new Intent(this, Main.class);
			
			ventana = new AlertDialog.Builder(this);
			ventana.setTitle(R.string.tit_v_enviado);
			ventana.setMessage(R.string.txt_email_olvido);
			ventana.setIcon(android.R.drawable.ic_dialog_email);
			ventana.setPositiveButton(R.string.btn_aceptar, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int boton) {
					
					startActivity(i_main);
				}
			});
			break;
			
		case NO_CORREO:
			
			ventana = new AlertDialog.Builder(this);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.txt_env_email_no_correo);
			ventana.setIcon(android.R.drawable.ic_dialog_info);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {

						}
					});
			break;
		}
			
		ventana.show();
		return ventana.create();
	}
}