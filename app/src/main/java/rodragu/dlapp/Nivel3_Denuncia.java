package rodragu.dlapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Nivel3_Denuncia extends Activity {
	private EditText ed_desc;
	private TextView txt_fecha, txt_tipo;
	private String usuario, tipo, descripcion, fecha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel3_denuncia);

			// RECOGEMOS LOS PARÁMETROS PASADO MEDIANTE INTENT
			Bundle extra = this.getIntent().getExtras();

			if (extra == null)
				return;

			fecha = extra.getString("fecha");
			tipo = extra.getString("tipo").toUpperCase();
			descripcion = extra.getString("descripcion");

			// LOCALIZAMOS LAS VISTAS
			txt_fecha = (TextView) findViewById(R.id.n3_fecha_denuncia);
			txt_tipo = (TextView) findViewById(R.id.n3_tipo_denuncia);
			ed_desc = (EditText) findViewById(R.id.n3_ed_desc);

			txt_fecha.setText(fecha);
			txt_tipo.setText(tipo);
			ed_desc.setText(descripcion);
	}
}