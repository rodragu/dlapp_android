package rodragu.dlapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Nivel2_Enviar_Email extends Activity {

	private static final int VACIO = 0;
	private static final int ENVIADO = 1;
	private static final int NO_CORREO = 2;
	private ProgressDialog pDialog;
	private String txt, usuario;
	private EditText ed_email;
	private Button btn_enviar;
	private AlertDialog.Builder ventana;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel2_enviar_email);

		// RECOGEMOS LOS PARÁMETROS PASADO MEDIANTE INTENT
		Bundle extra = this.getIntent().getExtras();

		if (extra == null)
			return;

		usuario = extra.getString("usuario");

		// LOCALIZAMOS LAS VISTAS
		ed_email = (EditText) findViewById(R.id.ed_email);
		btn_enviar = (Button) findViewById(R.id.n2_btn_enviar_email);

		btn_enviar.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {

				if (ed_email.getText().toString().equals("")) {
					showDialog(VACIO);
				} else {

					try {
						txt = "Por favor, contacten conmigo en el siguiente email: "
								+ ed_email.getText().toString();

						// INTENT PARA ENVIAR EL EMAIL
						final Intent i_email = new Intent(Intent.ACTION_SEND);

						final String[] to = { "denuncialaboralanonima@gmail.com" };

						i_email.setData(Uri.parse("mailto:"));
						i_email.putExtra(Intent.EXTRA_EMAIL, to);
						i_email.putExtra(Intent.EXTRA_SUBJECT,
								"Solicitud de contacto por email");
						i_email.putExtra(Intent.EXTRA_TEXT, txt);
						// i_email.setType("plain/text");
						i_email.setType("message/rfc822");

						asyncEnviar as = new asyncEnviar();
						as.execute(i_email);
						showDialog(ENVIADO);	
						
					} catch (android.content.ActivityNotFoundException ex) {
						showDialog(NO_CORREO);
					}
				}
			}
		});
	}
	
	class asyncEnviar extends AsyncTask<Intent, String, String> {

		String pass;

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Nivel2_Enviar_Email.this);
			pDialog.setMessage("Enviando email...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected String doInBackground(Intent... params) {
			
			startActivity(Intent.createChooser(params[0],"Send mail..."));
			return pass;
		}
		
		/*
		 * Una vez terminado doInBackground segun lo que halla ocurrido pasamos
		 * a la sig. activity o mostramos error
		 */
		protected void onPostExecute(String result) {

			Log.e("onPostExecute=", "" + result);
			
			
		}
	}

	public Dialog onCreateDialog(int id) {
		switch (id) {

		case VACIO:

			ventana = new AlertDialog.Builder(Nivel2_Enviar_Email.this);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.txt_email_vacio);
			ventana.setIcon(android.R.drawable.ic_dialog_info);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {

						}
					});
			break;

		case ENVIADO:

			ventana = new AlertDialog.Builder(Nivel2_Enviar_Email.this);
			ventana.setTitle(R.string.tit_v_enviado);
			ventana.setMessage(R.string.txt_email_enviado);
			ventana.setIcon(android.R.drawable.ic_dialog_email);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {
							// INTENT PARA VOLVER AL PRINCIPIO DE LA APP
							Log.i("Finished sending email...", "");
							final Intent i_nivel1 = new Intent(
									Nivel2_Enviar_Email.this, Nivel1.class);
							i_nivel1.putExtra("usuario", usuario);
							startActivity(i_nivel1);
						}
					});
			break;

		case NO_CORREO:

			ventana = new AlertDialog.Builder(Nivel2_Enviar_Email.this);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.txt_email_vacio);
			ventana.setIcon(android.R.drawable.ic_dialog_info);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {

						}
					});
			break;
		}

		ventana.show();
		return ventana.create();
	}
}