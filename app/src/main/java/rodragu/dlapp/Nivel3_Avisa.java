package rodragu.dlapp;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;

public class Nivel3_Avisa extends Activity {

	private static final int ENVIADO = 1;
	private static final int VACIO = 0;
	private ProgressDialog pDialog;
	private Button Enviar_Salir;
	private CheckBox chk_si, chk_no, chk_ccoo, chk_cgt, chk_cuadros, chk_ugt,
			chk_otros;
	private EditText Ed_Ampliar;
	private String usuario, sede, sindicato;
	private AlertDialog.Builder ventana;
	private Httppostaux post;
	private String IP_Server = "www.denuncialaboralanonima.esy.es/"; // IP DEL
																		// SERVIDOR
																		// WEB
	private String URL_connect = "http://" + IP_Server + "/addDenuncia.php"; // RUTA
																				// CON
																				// EL
																				// ARCHIVO
																				// QUE
																				// EJECUTA
																				// LA
																				// ACCI�N
																				// INSERTAR
																				// DENUNCIA

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel3_avisa);

		// RECOGEMOS LOS PAR�METROS PASADO MEDIANTE INTENT
		Bundle extra = this.getIntent().getExtras();

		if (extra == null)
			return;

		usuario = extra.getString("usuario");
		sede = extra.getString("sede");

		Toast.makeText(getApplicationContext(), "Usuario: " + usuario,
				Toast.LENGTH_LONG).show();

		// INICIALIZAMOS POST PARA OBTENER LA CONEXI�N CON EL SERVIDOR
		post = new Httppostaux();

		// LOCALIZAMOS LAS VISTAS
		Enviar_Salir = (Button) findViewById(R.id.btn_enviar_salir);
		chk_si = (CheckBox) findViewById(R.id.n3_txt_avisa_chk_si);
		chk_no = (CheckBox) findViewById(R.id.n3_txt_avisa_chk_no);
		chk_ccoo = (CheckBox) findViewById(R.id.chk_ccoo);
		chk_cgt = (CheckBox) findViewById(R.id.chk_cgt);
		chk_cuadros = (CheckBox) findViewById(R.id.chk_cuadros);
		chk_ugt = (CheckBox) findViewById(R.id.chk_ugt);
		chk_otros = (CheckBox) findViewById(R.id.chk_otros);
		Ed_Ampliar = (EditText) findViewById(R.id.n3_edit_avisa);

		// EVENTO CHECKBOX "S�"
		chk_si.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				if (chk_si.isChecked() == true) {
					chk_no.setEnabled(false);

					// HABILITAMOS PODER ELEGIR LOS SINDICATOS
					chk_ccoo.setEnabled(true);
					chk_cgt.setEnabled(true);
					chk_cuadros.setEnabled(true);
					chk_ugt.setEnabled(true);
					chk_otros.setEnabled(true);
				} else {

					chk_no.setEnabled(true);

					// DESHABILITAMOS PODER ELEGIR LOS SINDICATOS
					chk_ccoo.setEnabled(false);
					chk_cgt.setEnabled(false);
					chk_cuadros.setEnabled(false);
					chk_ugt.setEnabled(false);
					chk_otros.setEnabled(false);
				}
			}
		});

		// EVENTO CHECKBOX "NO"
		chk_no.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				if (chk_no.isChecked() == true) {
					chk_si.setEnabled(false);
					chk_ccoo.setEnabled(false);
					chk_cgt.setEnabled(false);
					chk_cuadros.setEnabled(false);
					chk_ugt.setEnabled(false);
					chk_otros.setEnabled(false);
				} else {
					chk_si.setEnabled(true);
				}
			}
		});

		// EVENTO BOT�N SALIR

		Enviar_Salir.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {

				int af = 0;

				// SI EL USUARIO NO MARCA NINGUNA OPCI�N (S� O NO)
				if (chk_si.isChecked() == false && chk_no.isChecked() == false) {
					showDialog(VACIO);

				} else {
					// SI EL USUARIO MARCA S�
					if (chk_si.isChecked() == true) {

						// SI EL USUARIO NO MARCA NING�N SINDICATO
						if (chk_ccoo.isChecked() == false
								&& chk_cgt.isChecked() == false
								&& chk_cuadros.isChecked() == false
								&& chk_ugt.isChecked() == false
								&& chk_otros.isChecked() == false) {
							showDialog(1);
						} else {
							if (chk_ccoo.isChecked() == true) {
								if (sindicato == "")
									sindicato = "CCOO";
								else
									sindicato += ", CCOO";
							}

							if (chk_cgt.isChecked() == true) {
								if (sindicato == "")
									sindicato = "CGT";
								else
									sindicato += ", CGT";
							}

							if (chk_cuadros.isChecked() == true) {
								if (sindicato == "")
									sindicato = "CUADROS Y MANDOS";
								else
									sindicato += ", CUADROS Y MANDOS";
							}

							if (chk_ugt.isChecked() == true) {
								if (sindicato == "")
									sindicato = "UGT";
								else
									sindicato += ", UGT";
							}

							if (chk_otros.isChecked() == true) {
								if (sindicato == "")
									sindicato = "OTROS";
								else
									sindicato += ", OTROS";
							}

							af = 1;
						}
					} else {
						sindicato = "NO AFILIADO";
						af = 1;
					}
					// Registramos la solicitud en la BBDD y vemos una ventana
					// que nos avisa de que la solicitud est� hecha y volvemos
					// al men�

					asyncInsertar as = new asyncInsertar();
					as.execute(usuario, sindicato, Ed_Ampliar.getText()
							.toString(), sede);
				}
			}
		});
	}

	public void insertarAviso(String usuario, String sindicato, String info,
			String sede) {

		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		postparameters2send.add(new BasicNameValuePair("fecha", fecha()));
		postparameters2send.add(new BasicNameValuePair("usuario", usuario));
		postparameters2send.add(new BasicNameValuePair("tipo","AVISO A UN REPRESENTANTE SINDICAL"));
		postparameters2send.add(new BasicNameValuePair("sindicato", sindicato));
		postparameters2send.add(new BasicNameValuePair("visita", "AVISO"));
		postparameters2send.add(new BasicNameValuePair("info", info));
		postparameters2send.add(new BasicNameValuePair("sede", sede));

		// realizamos una peticion y como respuesta obtenemos un array JSON
		post.httppostconnect(postparameters2send, URL_connect);
	}

	/*
	 * CLASE ASYNCTASK
	 * 
	 * usaremos esta para poder mostrar el dialogo de progreso mientras enviamos
	 * y obtenemos los datos podria hacerse lo mismo sin usar esto pero si el
	 * tiempo de respuesta es demasiado lo que podria ocurrir si la conexion es
	 * lenta o el servidor tarda en responder la aplicacion sera inestable.
	 * ademas observariamos el mensaje de que la app no responde.
	 */

	class asyncInsertar extends AsyncTask<String, String, String> {

		String user, pass;

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Nivel3_Avisa.this);
			pDialog.setMessage("Insertando la nueva denuncia....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			user = params[0];

			// Enviamos y recibimos y analizamos los datos en segundo plano.
			insertarAviso(params[0], params[1], params[2], params[3]);

			return user;
		}

		/*
		 * Una vez terminado doInBackground segun lo que halla ocurrido pasamos
		 * a la sig. activity o mostramos error
		 */
		protected void onPostExecute(String result) {

			pDialog.dismiss();// ocultamos progess dialog.

			Intent i_nivel1 = new Intent(Nivel3_Avisa.this, Nivel1.class);

			Log.e("onPostExecute=", "" + result);

			showDialog(ENVIADO);

			i_nivel1.putExtra("usuario", result);
			startActivity(i_nivel1);
		}
	}

	public Dialog onCreateDialog(int id) {

		final Intent i_opciones = new Intent(this, Main.class);

		switch (id) {

		case ENVIADO:

			ventana = new AlertDialog.Builder(this);
			ventana.setTitle(R.string.tit_mensaje_enviado);
			ventana.setMessage(R.string.txt_mensaje_enviado);
			ventana.setIcon(android.R.drawable.ic_dialog_alert);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int opcion) {

							startActivity(i_opciones);
						}
					});

			break;

		case VACIO:

			ventana = new AlertDialog.Builder(this);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.txt_email_vacio);
			ventana.setIcon(android.R.drawable.ic_dialog_info);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {

						}
					});
			break;
		}

		ventana.show();
		return ventana.create();
	}

	public String fecha() {
		Calendar fecha = Calendar.getInstance();
		String cad = fecha.get(Calendar.DAY_OF_MONTH) + "/"
				+ fecha.get(Calendar.MONTH) + "/" + fecha.get(Calendar.YEAR);

		return cad;
	}
}