package rodragu.dlapp;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

public class Nivel5_Denunciar extends Activity {

	private AlertDialog.Builder ventana;
	private ProgressDialog pDialog;
	private Button enviar;
	private EditText ed_info;
	private CheckBox chk_si, chk_no, chk_ccoo, chk_cgt, chk_cuadros, chk_ugt,
			chk_otros, chk_avisa_si, chk_avisa_no;
	private String sindicato = "", tipo, sede, visita, usuario;
	private static final int V_AFILIADO = 0;
	private static final int SINDICATO = 1;
	private static final int VISITA = 2;
	private static final int DENUNCIA = 3;
	private static final int NO_DENUNCIA = 4;
	private Httppostaux post;
	private String IP_Server = "www.denuncialaboralanonima.esy.es"; // IP DEL
																	// SERVIDOR
																	// WEB
	private String URL_connect = "http://" + IP_Server + "/addDenuncia.php"; // RUTA
																				// CON
																				// EL
																				// ARCHIVO
																				// QUE
																				// EJECUTA
																				// LA
																				// ACCI�N
																				// INSERTAR
																				// DENUNCIA

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel5_denunciar);

		// RECOGEMOS LOS PAR�METROS PASADO MEDIANTE INTENT
		Bundle extra = this.getIntent().getExtras();

		if (extra == null)
			return;

		usuario = extra.getString("usuario");
		sede = extra.getString("sede");
		tipo = extra.getString("tipo");

		// INICIALIZAMOS POST PARA OBTENER LA CONEXI�N CON EL SERVIDOR
		post = new Httppostaux();

		// LOCALIZAMOS LAS VISTAS DEL ARCHIVO XML

		enviar = (Button) findViewById(R.id.btn_enviar_salir);
		ed_info = (EditText) findViewById(R.id.ed_ampliar);
		chk_si = (CheckBox) findViewById(R.id.n5_chk_si);
		chk_no = (CheckBox) findViewById(R.id.n5_chk_no);
		chk_ccoo = (CheckBox) findViewById(R.id.chk_ccoo);
		chk_cgt = (CheckBox) findViewById(R.id.chk_cgt);
		chk_cuadros = (CheckBox) findViewById(R.id.chk_cuadros);
		chk_ugt = (CheckBox) findViewById(R.id.chk_ugt);
		chk_otros = (CheckBox) findViewById(R.id.chk_otros);
		chk_avisa_si = (CheckBox) findViewById(R.id.n5_chk_avisa_si);
		chk_avisa_no = (CheckBox) findViewById(R.id.n5_chk_avisa_no);

		chk_si.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				if (chk_si.isChecked() == true) {
					chk_no.setEnabled(false);
					chk_ccoo.setEnabled(true);
					chk_cgt.setEnabled(true);
					chk_cuadros.setEnabled(true);
					chk_ugt.setEnabled(true);
					chk_otros.setEnabled(true);
				} else {
					chk_no.setEnabled(true);
					chk_ccoo.setEnabled(false);
					chk_cgt.setEnabled(false);
					chk_cuadros.setEnabled(false);
					chk_ugt.setEnabled(false);
					chk_otros.setEnabled(false);
				}
			}
		});

		chk_no.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				if (chk_no.isChecked() == true) {
					chk_si.setEnabled(false);
					chk_ccoo.setEnabled(false);
					chk_cgt.setEnabled(false);
					chk_cuadros.setEnabled(false);
					chk_ugt.setEnabled(false);
					chk_otros.setEnabled(false);
				} else {
					if (chk_si.isChecked() == false) {
						chk_si.setEnabled(true);
						chk_ccoo.setEnabled(false);
						chk_cgt.setEnabled(false);
						chk_cuadros.setEnabled(false);
						chk_ugt.setEnabled(false);
						chk_otros.setEnabled(false);
					}
				}
			}
		});

		chk_avisa_si.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				if (chk_avisa_si.isChecked() == true) {
					chk_avisa_no.setEnabled(false);
				} else
					chk_avisa_no.setEnabled(true);
			}
		});

		chk_avisa_no.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				if (chk_avisa_no.isChecked() == true) {
					chk_avisa_si.setEnabled(false);
				} else
					chk_avisa_si.setEnabled(true);
			}
		});

		enviar.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {

				int af = 0, vi = 0;

				// SI EL USUARIO NO MARCA NINGUNA OPCI�N (S� O NO)
				if (chk_si.isChecked() == false && chk_no.isChecked() == false) {
					showDialog(V_AFILIADO);
					af = 0;
				} else {
					// SI EL USUARIO MARCA S�

					if (chk_si.isChecked() == true) {

						// SI EL USUARIO NO MARCA NING�N SINDICATO

						if (chk_ccoo.isChecked() == false
								&& chk_cgt.isChecked() == false
								&& chk_cuadros.isChecked() == false
								&& chk_ugt.isChecked() == false
								&& chk_otros.isChecked() == false) {
						} else {
							if (chk_ccoo.isChecked() == true) {
								if (sindicato == "")
									sindicato = "CCOO";
								else
									sindicato += ", CCOO";
							}

							if (chk_cgt.isChecked() == true) {
								if (sindicato == "")
									sindicato = "CGT";
								else
									sindicato += ", CGT";
							}

							if (chk_cuadros.isChecked() == true) {
								if (sindicato == "")
									sindicato = "CUADROS Y MANDOS";
								else
									sindicato += ", CUADROS Y MANDOS";
							}

							if (chk_ugt.isChecked() == true) {
								if (sindicato == "")
									sindicato = "UGT";
								else
									sindicato += ", UGT";
							}

							if (chk_otros.isChecked() == true) {
								if (sindicato == "")
									sindicato = "OTROS";
								else
									sindicato += ", OTROS";
							}

							af = 1;
						}
					} else {
						sindicato = "No afiliado";
						af = 1;
					}
				}

				// CHECKBOX VISITA REPRESENTANTE VAC�OS

				if (chk_avisa_si.isChecked() == false
						&& chk_avisa_no.isChecked() == false) {
					showDialog(VISITA);
					vi = 0;
				} else // SI SE HA ELEGIDO UNA OPCI�N...
				{
					if (chk_avisa_no.isChecked() == true) {
						visita = "NO";
						vi = 1;
					}

					else {
						visita = "S�";
						vi = 1;
					}
				}

				if (af == 1 && vi == 1) {

					asyncInsertar as = new asyncInsertar();
					as.execute(usuario, tipo.toUpperCase(), sindicato, visita,
							ed_info.getText().toString(), sede);
				}
			}
		});
	}

	// FUNCI�N CON LA QUE INSERTAMOS UNA DENUNCIA EN LA BBDD

	public String insertarDenuncia(String usuario, String tipo,
			String sindicato, String visita, String info, String sede) {

		ArrayList<NameValuePair> postparameters2send = new ArrayList<NameValuePair>();

		postparameters2send.add(new BasicNameValuePair("fecha", fecha()));
		postparameters2send.add(new BasicNameValuePair("usuario", usuario));
		postparameters2send.add(new BasicNameValuePair("tipo", tipo));
		postparameters2send.add(new BasicNameValuePair("sindicato", sindicato));
		postparameters2send.add(new BasicNameValuePair("visita", visita));
		postparameters2send.add(new BasicNameValuePair("info", info));
		postparameters2send.add(new BasicNameValuePair("sede", sede));

		return post.httppostconnect(postparameters2send, URL_connect);
	}

	/*
	 * CLASE ASYNCTASK
	 * 
	 * usaremos esta para poder mostrar el dialogo de progreso mientras enviamos
	 * y obtenemos los datos podria hacerse lo mismo sin usar esto pero si el
	 * tiempo de respuesta es demasiado lo que podria ocurrir si la conexion es
	 * lenta o el servidor tarda en responder la aplicacion sera inestable.
	 * ademas observariamos el mensaje de que la app no responde.
	 */

	class asyncInsertar extends AsyncTask<String, String, String> {

		String user, pass;

		protected void onPreExecute() {
			// para el progress dialog
			pDialog = new ProgressDialog(Nivel5_Denunciar.this);
			pDialog.setMessage("Insertando la nueva denuncia....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			user = params[0];

			// Enviamos y recibimos y analizamos los datos en segundo plano.
			insertarDenuncia(params[0], params[1], params[2], params[3],
					params[4], params[5]);

			return user;
		}

		/*
		 * Una vez terminado doInBackground segun lo que halla ocurrido pasamos
		 * a la sig. activity o mostramos error
		 */
		protected void onPostExecute(String result) {

			pDialog.dismiss();// ocultamos progess dialog.
			
			showDialog(DENUNCIA);
		}
	}

	public Dialog onCreateDialog(int id) {
		final Intent i_main = new Intent(this, Main.class);

		switch (id) {

		case V_AFILIADO:

			ventana = new AlertDialog.Builder(this);
			ventana.setIcon(android.R.drawable.ic_dialog_alert);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.v_afiliado);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {

						}
					});
			break;

		case SINDICATO:

			ventana = new AlertDialog.Builder(this);
			ventana.setIcon(android.R.drawable.ic_dialog_alert);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.v_sindicato);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {

						}
					});
			break;

		case VISITA:

			ventana = new AlertDialog.Builder(this);
			ventana.setIcon(android.R.drawable.ic_dialog_alert);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.v_visita);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {

						}
					});
			break;

		case DENUNCIA:

			ventana = new AlertDialog.Builder(this);
			ventana.setIcon(android.R.drawable.ic_dialog_info);
			ventana.setTitle(R.string.tit_v_enhorabuena);
			ventana.setMessage(R.string.denuncia_exito);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {
							
							Intent i_nivel1 = new Intent(Nivel5_Denunciar.this, Nivel1.class);
							i_nivel1.putExtra("usuario", usuario);
							startActivity(i_nivel1);
						}
					});
			break;

		case NO_DENUNCIA:

			ventana = new AlertDialog.Builder(this);
			ventana.setIcon(android.R.drawable.ic_dialog_alert);
			ventana.setTitle(R.string.atencion);
			ventana.setMessage(R.string.denuncia_fallo);
			ventana.setPositiveButton(R.string.btn_aceptar,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int boton) {
							
							startActivity(i_main);
						}
					});
			break;
		}
		ventana.show();
		return ventana.create();
	}

	public String fecha() {
		Calendar fecha = Calendar.getInstance();
		String cad = fecha.get(Calendar.DAY_OF_MONTH) + "/"
				+ fecha.get(Calendar.MONTH) + "/" + fecha.get(Calendar.YEAR);

		return cad;
	}
}