package rodragu.dlapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Main extends Activity {
	
	private Button comenzar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);	
	
		final Intent i_loguearse = new Intent(this, Loguearse.class);
		comenzar = (Button)findViewById(R.id.main_btn);
		
		comenzar.setOnClickListener(new OnClickListener(){
			public void onClick(View view) {
				startActivity(i_loguearse);
			}
		});
	}
}