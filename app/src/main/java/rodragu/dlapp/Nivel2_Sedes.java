package rodragu.dlapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Nivel2_Sedes extends Activity {

	private static int opcion;
	private static final int DENUNCIAR = 0;
	private static final int AVISAR = 1;
	private static final int NOVEDADES = 2;
	private AlertDialog.Builder ventana;
	private Button sede, proxim, contacto;
	private String usuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel2_sedes);
		
			// Recogemos los par�metros pasados a la actividad mediante el
			// Intent
			final Bundle extra = this.getIntent().getExtras();

			if (extra == null)
				return;

			usuario = extra.getString("usuario");
			opcion = extra.getInt("opcion");

			sede = (Button) findViewById(R.id.n1_sede_btn);
			//proxim = (Button) findViewById(R.id.n1_btn_proxim);
			//contacto = (Button) findViewById(R.id.n1_btn_contacto);

			// final Intent i_proxim = new Intent(this, Proxim.class);

			sede.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Mostramos una ventana con el listado para elegir la sede
					// y luego ir a la pantalla Nivel2
					onCreateDialog2(opcion, usuario);
				}
			});
	}

	public Dialog onCreateDialog2(int id, final String usuario) {

		final Intent i_n2;

		ventana = new AlertDialog.Builder(this);
		ventana.setIcon(android.R.drawable.ic_dialog_alert);
		ventana.setTitle("Elija la sede en la que trabaja:");

		switch (id) {

		case DENUNCIAR:

			i_n2 = new Intent(this, Nivel3_Situaciones.class);

			ventana.setItems(R.array.sedes,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int opcion) {
							switch (opcion) {
							case 0:
								i_n2.putExtra("sede",
										"Renault Espa�a - Palencia");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
								break;

							case 1:
								i_n2.putExtra("sede",
										"Renault Espa�a - Sevilla");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
								break;

							case 2:
								i_n2.putExtra("sede",
										"Renault Espa�a - Valladolid");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
							}
						}
					});
			break;

		case AVISAR:

			i_n2 = new Intent(this, Nivel3_Avisa.class);

			ventana.setItems(R.array.sedes,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int opcion) {
							switch (opcion) {
							case 0:
								i_n2.putExtra("sede",
										"Renault Espa�a - Palencia");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
								break;

							case 1:
								i_n2.putExtra("sede",
										"Renault Espa�a - Sevilla");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
								break;

							case 2:
								i_n2.putExtra("sede",
										"Renault Espa�a - Valladolid");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
							}
						}
					});
			break;

		

		case NOVEDADES:

			i_n2 = new Intent(this, Nivel2_Novedades.class);

			ventana.setItems(R.array.sedes,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int opcion) {
							switch (opcion) {
							case 0:

								i_n2.putExtra("sede",
										"Renault Espa�a - Palencia");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
								break;

							case 1:
								i_n2.putExtra("sede",
										"Renault Espa�a - Sevilla");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
								break;

							case 2:
								i_n2.putExtra("sede",
										"Renault Espa�a - Valladolid");
								i_n2.putExtra("usuario", usuario);
								startActivity(i_n2);
							}
						}
					});
			break;
		}

		ventana.show();
		return ventana.create();
	}
}